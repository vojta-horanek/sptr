use colored::*;
use rustyline::error::ReadlineError;
use rustyline::Editor;

mod cmd;
mod data;
/* TODO

- DONE Implement adding new items
- Implement removing items
- Implement editing items
    (items = transactions/accounts)
- Add settings to enable/disable color
- Maybe add command history

*/

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let mut read_line = Editor::<()>::new();

    let mut interactive = true;
    if args.len() > 1 {
        interactive = false;
    }

    // Get HOME env var
    let mut config_file = std::env::var("HOME").unwrap();

    // Create config directory if not created already
    config_file.push_str("/.config/sptr");
    std::fs::create_dir_all(config_file.clone()).unwrap();

    config_file.push_str("/data.json");
    let data = data::Data::new(&config_file).unwrap_or(data::Data::empty());

    let mut command_line: cmd::Commands = cmd::Commands::new(data, &config_file);

    let mut input = String::new();

    loop {
        let command;
        if interactive {
            let line = read_line.readline(&"sptr> ".blue().bold().to_string());
            match line {
                Ok(line) => {
                    read_line.add_history_entry(line.as_str());
                    input = line;
                    // Parser expects new line at the end of a command
                    input.push('\n');
                }
                Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => {
                    break;
                }
                Err(err) => {
                    println!("E: {:?}", err);
                    break;
                }
            }

            command = match cmd::parse_input(&input) {
                Ok(comm) => comm,
                Err(e) => {
                    input.pop(); // Remove new line from input
                    eprintln!("E: Cannot parse input: {} (`{}`)", e, input);
                    input.clear();
                    continue;
                }
            };
            if command.is_empty() {
                continue;
            }
        } else {
            command = args[1..].to_vec();
        }

        match command_line.run_command(&command) {
            Ok(_) => (),
            Err(error) => eprintln!("E: {}", error),
        }
        input.clear();

        // Exiting or not running in interactive mode
        if command_line.should_exit || !interactive {
            break;
        }
    }
}

use colored::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt::Write as FmtWrite;
use std::fmt::{self, Display, Formatter};
use std::fs::File;
use std::fs::OpenOptions;
use std::io::BufReader;
use std::io::BufWriter;

#[derive(Serialize, Deserialize)]
pub struct Account {
    pub name: String,
    pub balance: f64,
    pub currency: String,
    pub id: u32,
    pub color: String,
}

#[derive(Serialize, Deserialize)]
pub struct Transaction {
    pub description: String,
    pub total: f64,
    pub account_id: u32,
    pub date: String,
}

#[derive(Serialize, Deserialize)]
pub struct Data {
    #[serde(with = "account")]
    pub accounts: HashMap<u32, Account>,
    pub transactions: Vec<Transaction>,
}

impl Display for Account {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "Name: {}\nBalance: {}\nCurrency: {}",
            self.name, self.balance, self.currency
        )
    }
}

impl Display for Transaction {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{} <= {} [{}]", self.total, self.description, self.date)
    }
}

mod account {
    use super::Account;

    use std::collections::HashMap;

    use serde::de::{Deserialize, Deserializer};
    use serde::ser::Serializer;

    pub fn serialize<S>(map: &HashMap<u32, Account>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.collect_seq(map.values())
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<HashMap<u32, Account>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let mut map = HashMap::new();
        for item in Vec::<Account>::deserialize(deserializer)? {
            map.insert(item.id, item);
        }
        Ok(map)
    }
}

impl Data {
    pub fn new(file: &str) -> Result<Data, &str> {
        Data::get_saved_data(file)
    }

    pub fn empty() -> Data {
        Data {
            accounts: HashMap::new(),
            transactions: Vec::new(),
        }
    }

    pub fn get_account_str(&self, account: &Account) -> String {
        let mut output = String::new();
        write!(
            &mut output,
            "{} {} in {}",
            if account.balance < 0.0 {
                account.balance.to_string().red().bold()
            } else {
                account.balance.to_string().green().bold()
            },
            account.currency,
            account.name.color(account.color.clone())
        )
        .unwrap();
        output
    }

    pub fn get_transaction_str(&self, transaction: &Transaction) -> String {
        let mut output: String = String::new();
        let bank = self.get_account_by_id(&transaction.account_id);
        match bank {
            Some(bank) => {
                write!(
                    &mut output,
                    "{} {} ({})\n\t→ [{}] {}",
                    if transaction.total < 0.0 {
                        transaction.total.to_string().red().bold()
                    } else {
                        transaction.total.to_string().green().bold()
                    },
                    bank.currency,
                    transaction.date,
                    bank.name.color(bank.color.clone()),
                    transaction.description,
                )
                .unwrap();
            }
            None => {
                write!(
                    &mut output,
                    "E: Account id `{}` not found for transaction `{}`",
                    transaction.account_id, transaction.description,
                )
                .unwrap();
            }
        }
        output
    }

    pub fn get_saved_data(file: &str) -> Result<Data, &str> {
        let save_file = match File::open(file) {
            Ok(x) => x,
            Err(_) => return Err("Could not open file"),
        };
        let buff_reader = BufReader::new(save_file);
        match serde_json::from_reader::<_, Data>(buff_reader) {
            Ok(x) => Ok(x),
            Err(_) => Err("Could not parse file"),
        }
    }

    pub fn get_account_by_id(&self, id: &u32) -> Option<&Account> {
        self.accounts.get(id)
    }

    pub fn get_account_id_by_name(&self, name: &str) -> Option<u32> {
        self.accounts
            .iter()
            .filter(|(_, account)| &account.name == name)
            .map(|(id, _)| *id)
            .nth(0)
    }

    pub fn add_transaction(&mut self, transaction: Transaction) {
        self.transactions.push(transaction)
    }

    pub fn modify_balance(&mut self, account: u32, total: f64) -> Result<(), String> {
        match self.accounts.get_mut(&account) {
            Some(acc) => acc.balance += total,
            None => {
                return Err(format!(
                    "Cannot add to balance: account with id `{}` not found",
                    account
                ))
            }
        };
        Ok(())
    }

    pub fn save_data(&self, file: &str) -> Result<(), &str> {
        let save_file = match OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(file)
        {
            Ok(x) => x,
            Err(_) => return Err("Could not open file"),
        };
        let buf_writer = BufWriter::new(save_file);
        match serde_json::to_writer_pretty(buf_writer, self) {
            Ok(_) => Ok(()),
            Err(_) => Err("Could not save file"),
        }
    }
}

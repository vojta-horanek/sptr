use crate::data;
use chrono::Utc;
use rustyline::error::ReadlineError;
use rustyline::Editor;

pub struct Commands {
    pub should_exit: bool,
    data: data::Data,
    config_file: String,
}

impl Commands {
    pub fn new(data: data::Data, config_file: &str) -> Commands {
        Commands {
            should_exit: false,
            data: data,
            config_file: config_file.to_string(),
        }
    }
    pub fn run_command(&mut self, command: &Vec<String>) -> Result<(), String> {
        if command[0] == "exit" {
            self.should_exit = true;
            return Ok(());
        }
        let args = &command[1..].to_vec();
        match command[0].as_str() {
            "show" | "ls" => self.show(args),
            "load" => self.load(args),
            "save" => self.save(args),
            "add" => self.add(args),
            "transaction" | "new" => self.transaction(),
            "account" => self.account(),
            "exit" | "quit" => {
                self.should_exit = true;
                Ok(())
            }
            x => Err(format!("Command not found: `{}`", x)),
        }
    }

    fn add(&mut self, arg: &Vec<String>) -> Result<(), String> {
        let error: String = String::from("Wrong format, use `add [transaction [total] [description] [account_name/id] (date) | account [name] [color] [currency] (balance)]`");
        let mut arg_iter = arg.iter();
        match arg_iter.next() {
            Some(x) => {
                if "transaction".starts_with(x) {
                    // get and parse total
                    let total: f64 = match arg_iter.next() {
                        Some(total) => match total.parse() {
                            Ok(val) => val,
                            Err(_) => return Err(format!("`{}` is not a number", total)),
                        },
                        None => return Err(error),
                    };
                    // get description, else error out
                    let description = match arg_iter.next() {
                        Some(desc) => desc.clone(),
                        None => return Err(error),
                    };
                    // get account id from provided account name or account id
                    let account_id: u32 = match arg_iter.next() {
                        Some(name) => match name.parse() {
                            Ok(id) => match self.data.get_account_by_id(&id) {
                                Some(_) => id,
                                None => return Err(format!("Account with id `{}` not found", id)),
                            },
                            Err(_) => match self.data.get_account_id_by_name(name) {
                                Some(account) => account,
                                None => return Err(format!("Account not found: {}", name)),
                            },
                        },
                        None => return Err(error),
                    };
                    // get date from argument, use todays date if not provided
                    let date = match arg_iter.next() {
                        Some(x) => x.clone(),
                        None => Utc::now().naive_local().format("%d/%m/%Y").to_string(),
                    };

                    self.data.add_transaction(data::Transaction {
                        total: total,
                        account_id: account_id,
                        description: description.to_string(),
                        date: date.to_string(),
                    });

                    self.data.modify_balance(account_id, total)?;
                    Ok(())
                } else if "account".starts_with(x) {
                    // TODO Implement
                    Ok(())
                } else {
                    Err(error)
                }
            }
            None => Err(error),
        }
    }

    fn transaction(&mut self) -> Result<(), String> {
        let options = vec!["Total", "Description", "Account ID/Name", "Date (current)"];
        let mut inputs = self.option_input(&options.iter().map(|str| str.to_string()).collect())?;
        inputs.insert(0, "transaction".to_string());
        self.add(&inputs)
    }

    fn account(&mut self) -> Result<(), String> {
        let options = vec!["Name", "Currency", "Color", "Balance (0)"];
        let mut inputs = self.option_input(&options.iter().map(|str| str.to_string()).collect())?;
        inputs.insert(0, "account".to_string());
        self.add(&inputs)
    }

    fn option_input(&self, options: &Vec<String>) -> Result<Vec<String>, &str> {
        let mut params: Vec<String> = Vec::new();
        let mut read_line = Editor::<()>::new();

        for option in options {
            let readline = read_line.readline(&format!("{}: ", option));
            match readline {
                Ok(line) => {
                    if !line.is_empty() {
                        params.push(line);
                    }
                }
                Err(ReadlineError::Eof) | Err(ReadlineError::Interrupted) => {
                    return Err("Terminted before getting all values")
                }
                Err(err) => {
                    println!("Error while reading: {:?}", err);
                    return Err("Terminted before getting all values");
                }
            }
        }
        Ok(params)
    }

    fn load(&mut self, arg: &Vec<String>) -> Result<(), String> {
        let what = arg.get(0);
        match what {
            Some(x) => {
                self.data = data::Data::get_saved_data(x)?;
            }
            None => {
                self.data = data::Data::get_saved_data(&self.config_file)?;
            }
        }
        Ok(())
    }

    fn save(&self, arg: &Vec<String>) -> Result<(), String> {
        let what = arg.get(0);
        match what {
            Some(x) => {
                self.data.save_data(x)?;
            }
            None => {
                self.data.save_data(&self.config_file)?;
            }
        }
        Ok(())
    }

    fn show(&self, arg: &Vec<String>) -> Result<(), String> {
        let what = arg.get(0);
        match what {
            Some(x) => {
                if "transactions".starts_with(x) {
                    self.print_transactions();
                } else if "accounts".starts_with(x) {
                    self.print_accounts();
                } else {
                    return Err("Invalid show argument".to_string());
                }
            }
            None => {
                self.print_transactions();
                if self.data.transactions.len() != 0 && self.data.accounts.len() != 0 {
                    println!("{:=<1$}", "", 40);
                }
                self.print_accounts();
            }
        }
        Ok(())
    }

    fn print_transactions(&self) {
        for (i, t) in self.data.transactions.iter().enumerate() {
            println!("{}", self.data.get_transaction_str(t));
            if i != self.data.transactions.len() - 1 {
                println!("{:-<1$}", "", 40);
            }
        }
    }

    fn print_accounts(&self) {
        for a in &self.data.accounts {
            println!("{}", self.data.get_account_str(a.1));
        }
    }
}

pub fn parse_input(input: &String) -> Result<Vec<String>, String> {
    #[derive(PartialEq)]
    enum State {
        InsideWord,
        InsideQuote,
        Outside,
    }
    let mut current_state = State::Outside;
    let mut word = String::new();
    let mut command: Vec<String> = Vec::new();
    for c in input.chars() {
        match c {
            ' ' | '\n' => {
                match current_state {
                    State::Outside => continue,
                    State::InsideQuote => {
                        if c == ' ' {
                            word.push(c)
                        } else {
                            return Err("Unexpected newline after quote".to_string());
                        }
                    }
                    State::InsideWord => {
                        // Word finished
                        current_state = State::Outside;
                        command.push(word);
                        word = String::new();
                    }
                }
            }
            // Only support double quotes, easier to parse
            //'\"' | '\'' => {
            '\"' => {
                if current_state == State::InsideQuote {
                    current_state = State::InsideWord;
                } else if current_state == State::InsideWord {
                    return Err("Unexpected quote inside of word".to_string());
                } else {
                    current_state = State::InsideQuote;
                }
            }
            _ => {
                if current_state == State::Outside {
                    // New word
                    current_state = State::InsideWord;
                }
                word.push(c);
            }
        }
    }
    Ok(command)
}
